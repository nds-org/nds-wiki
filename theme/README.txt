= NDS theme for MoinMoin =

This is a theme for the MoinMoin wiki enigne.

== Instalation ==

To install this theme, put the following files into directories the
moin installation as follows:

 *  nds.py ==> [wiki]/data/plugin/theme 
 *  PageActions.py ==> [wiki]/data/plugin/action
 *  nds (directory) ==> [MoinMoin/web/static]/htdocs/nds
 *  wikilogo.png ==> [MoinMoin/web/static]/htdocs/common 

== License ==

This theme is licensed under GPL, see the LICENSE file you 
got with MoinMoin for details. 

It is based on the Mandarin theme by Radomir Dopieralski
<moin@sheep.art.pl>. 
