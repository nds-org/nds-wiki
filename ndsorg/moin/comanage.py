"""
An authentication module for matching an authenticated identity to one 
stored in CoManage user database.  

This module requires that the incoming user_obj has its auth_username property 
set to a user identifier string that they were authenticated as and 
its valid property set to True (indicating that authentication was successful). 
"""
from MoinMoin import log
logging = log.getLogger(__name__)

from MoinMoin import user
from MoinMoin.auth import BaseAuth
from MoinMoin.auth import CancelLogin, ContinueLogin
from MoinMoin.auth import MultistageFormLogin, MultistageRedirectLogin
from MoinMoin.auth import get_multistage_continuation_url

class CoManageAuth(BaseAuth):

    def __init__(self, require_success=True):
        BaseAuth.__init__(self)
        self._success_required = require_success

    def _quit_login(self, user_obj, message=None):
        if self._success_required:
            return CancelLogin(message)
        return ContinueLogin(user_obj, message)

    def login(self, request, user_obj, **kw):
        _ = request.getText

        if not user_obj or not user_obj.auth_username or not user_obj.valid:
            return self._quit_login(_("User has not authenticated"))

        authname = user_obj.auth_username
        if not authname.startswith("DC="):
            # unexpected format (TODO: be more modular)
            logging.fail("Unexpected authenticated name format: "+authname)
            return Continue(user_obj, 
                            _("Unrecognizable authenticated username"))

        user_obj = self.match_user(authname)
        dn = DN.from_str(authname)


        # TODO: map it to a comanage user and adjust the user_obj accordingly

        return ContinueLogin(u)


