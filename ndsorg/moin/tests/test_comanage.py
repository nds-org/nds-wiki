from __future__ import absolute_import
import sys, os, re, pytest

from OpenSSL import crypto
from .. import comanage as comanage
from MoinMoin.auth import ContinueLogin, CancelLogin, MultistageRedirectLogin

test_dir = os.path.join(os.path.dirname(comanage.__file__), "tests")

