from __future__ import absolute_import
import sys, os, re, pytest

from OpenSSL import crypto
from .. import cilogon as cilogon
from MoinMoin.auth import ContinueLogin, CancelLogin, MultistageRedirectLogin
from MoinMoin.web.contexts import AllContext
from MoinMoin.web.request import TestRequest
from MoinMoin.user import User

test_str = 'DC=org/DC=cilogon/C=US/O=University of Illinois at Urbana-Champaign/CN=Raymond Plante A16601'

test_dir = os.path.join(os.path.dirname(cilogon.__file__), "tests")

def test_extract_DN():
    with open(os.path.join(test_dir,"rplante.pem")) as fd:
        cert = fd.read()
    assert cilogon.extract_DN(cert) == test_str

def test_handle_cilogon_identity():
    with open(os.path.join(test_dir,"rplante.pem")) as fd:
        cert = fd.read()

    user_obj = User(AllContext(TestRequest()))

    class hresp(object):
        def __init__(self, data):
            self.content = data

    id_data = hresp(cert)

    user_obj = cilogon.handle_cilogon_identity(user_obj, id_data)
    assert user_obj.auth_username == cilogon.extract_DN(cert)
    assert user_obj.valid


