from __future__ import absolute_import
import sys, os, re, pytest

from .. import oath1 as oauth1
from MoinMoin.auth import ContinueLogin, CancelLogin, MultistageRedirectLogin

ckey = 'key'
csecret = 'secret'
base = 'http://term.ie/oauth/example'
request_url = 'request_token.php'
access_url = 'access_token.php'
id_url = 'echo_api.php?user=joe'

oauth1.logging.setLevel(oauth1.logging.DEBUG)

class TestOAuth1(object):

    def setup_method(self, method):
        self.o1 = oauth1.Oath1("http://my.net/oath", ckey, csecret)

    def test_urljoin(self):
        assert self.o1._urljoin("http://my.net/schema", "Person/name") == \
            "http://my.net/schema/Person/name"
        assert self.o1._urljoin("http://my.net/schema/", "Person/name") == \
            "http://my.net/schema/Person/name"
        assert self.o1._urljoin("http://my.net/schema", "/Person/name") == \
            "http://my.net/Person/name"
        assert self.o1._urljoin("http://my.net/schema", "http://schema.org/Person/name") == \
            "http://schema.org/Person/name"

        try:
            self.o1._urljoin("http://my.net/schema", "file:/Person/name")
            pytest.fail("failed to reject file scheme")
        except ValueError, ex:
            assert "unsupported scheme" in str(ex)

        try:
            self.o1._urljoin("http://my.net/schema", "file:/Person/name")
            pytest.fail("failed to reject file scheme")
        except ValueError, ex:
            assert "unsupported scheme" in str(ex)
        assert self.o1._urljoin("http://my.net/schema", "file:/Person/name", list(self.o1.ok_schemes)+["file"]) == \
            "file:/Person/name"

        try:
            self.o1._urljoin("http://my.net/schema", "https:/Person/name")
            pytest.fail("failed to reject file scheme")
        except ValueError, ex:
            assert "bad host" in str(ex)

    def test_defctor(self):
        assert self.o1._init_url == "http://my.net/oath/initiate"
        assert self.o1._auth_url == "http://my.net/delegate"
        assert self.o1._tok_url == "http://my.net/oath/token"
        assert self.o1._id_url == "http://my.net/oath/getcert"

    def set_for_net_test(self, postlogin=False):
        self.o1 = oauth1.Oath1(base, ckey, csecret, None,
                               request_url, "login", access_url, id_url)

        from werkzeug.contrib.sessions import Session
        from MoinMoin.web.request import TestRequest
        from MoinMoin.web.contexts import AllContext
        from MoinMoin.user import User
        if postlogin:
            self.request = TestRequest("/login", 
                     "oauth_verifier=verifier&action=login&login=1&stage=oath1")
            self.context = AllContext(self.request)
            self.context.session = \
                Session({"oauth1_request_token": "requestkey",
                         "oauth1_request_secret": "requestsecret"}, 
                        1, False)
        else:
            self.request = TestRequest("/login", "action=login&login=1")
            self.context = AllContext(self.request)
            self.context.session = Session({}, 1, True)

        self.context.lang = 'en'
        self.context.cfg.cookie_lifetime = (1, 1)
        self.user = None

    def test_need_anon(self):
        self.set_for_net_test()
        self.context.cfg.cookie_lifetime = (0, 1)
        result = self.o1.login(self.context, self.user)
        assert isinstance(result, CancelLogin)
        assert "Insufficient configuration" in result.message

    def test_require_success_not(self):
        self.set_for_net_test()
        self.o1 = oauth1.Oath1(base, ckey, csecret, None,
                               request_url, "login", access_url, id_url,
                               require_success=False)
        self.context.cfg.cookie_lifetime = (0, 1)
        result = self.o1.login(self.context, self.user)
        assert isinstance(result, ContinueLogin)
        assert "Insufficient configuration" in result.message

    @pytest.mark.webtest
    def test_net_login1(self):
        self.set_for_net_test()

        # pytest.set_trace()
        result = self.o1.login(self.context, self.user)
        assert self.context.session['oauth1_request_token'] == 'requestkey'
        assert self.context.session['oauth1_request_secret'] == 'requestsecret'
        assert isinstance(result, MultistageRedirectLogin)
        assert result.redirect_to == base+'/login?oauth_token=requestkey'
        # pytest.fail('showing log messages')

    @pytest.mark.webtest
    def test_net_login2(self):
        self.set_for_net_test(True)
        assert self.context.session['oauth1_request_token'] == 'requestkey'
        assert self.context.session['oauth1_request_secret'] == 'requestsecret'
        
        # pytest.set_trace()
        result = self.o1.login(self.context, self.user, multistage=self.o1.name)
        assert isinstance(result, CancelLogin)
        assert result.message == 'Unidentified user'
        # pytest.fail('showing log messages')

