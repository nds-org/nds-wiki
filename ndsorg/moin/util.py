"""
common utilities for use by authentication plugins
"""

class DN(object):
    """
    a representation of a X509 distinguished name which identifies subjects
    and issuers within an X509 certificate.  

    This contains the same information as an OpenSSL.crypto.X509Name but allows
    conversion back and forth between the string format and its components.  
    """
    _preforder = ["DC", "C", "ST", "L", "O", "OU", "CN"]

    def __init__(self, components):
        """
        create an instance from the components returned from 
        OpenSSL.crypto.X509Name.get_components().  
        """
        if not isinstance(components, list):
            raise ValueError("DN() expects a list of tuples; got: "+
                             str(components))
        self._comp = {}
        self._order = []
        for c in components:
            if not isinstance(c, tuple):
                raise ValueError("DN(): non-tuple found in component list: "+
                                 str(c))
            try:
                self._order.append(c[0])
                if self._comp.has_key(c[0]):
                    self._comp[c[0]].append(c[1])
                else:
                    self._comp[c[0]] = [c[1]]
                
            except IndexError, e:
                raise ValueError("DN(): each component tuple must have 2 "+
                                 "elements; got: " + str(c))

    def components(self):
        """
        return the DN parts as a list of 2-tuples.
        """
        out = []

        for c in self._preforder:
            out.extend(self.list_component(c))
        for c in self._order:
            if c not in self._preforder:
                out.extend(self.list_component(c))
        return out

    def list_component(self, rdn):
        out = []
        if self._comp.has_key(rdn):
            for v in self._comp[rdn]:
                out.append((rdn, v))
        return out;

    def __str__(self):
        return "/".join(map(lambda c: "=".join(c), self.components()))

    def get_component(self, rdn):
        """
        return the component with a given (relative distinguished) name
        """
        out = self._comp.get(rdn)
        if isinstance(out, list) and len(out)==1:
            return out[0]
        return out

    def set_component(self, rdn, val):
        """
        set the value of a component with a given (relative distinguished) 
        name, overwriting any previous values.  The given value may be a 
        list of values to set to.  
        """
        if rdn in self._comp:
            del self._comp[rdn]
        self.add_component(rdn, val)

    def add_component(self, rdn, val):
        """
        add an additional value for a component with a given (relative 
        distinguished) name.  The given value may be a list of values.
        """
        if rdn not in self._comp:
            self._comp[rdn] = []
        if not hasattr(val, "__iter__"):
            val = [val]
        self._comp[rdn].extend(val)

    @property
    def domainName(self):
        """
        The country name (DC) component
        """
        return self.get_component('DC')
    @domainName.setter
    def domainName(self, val):
        self.set_component('DC', val)
    @domainName.deleter
    def domainName(self):
        del self._comp['DC']

    @property
    def countryName(self):
        """
        The country name (C) component
        """
        return self.get_component('C')
    @countryName.setter
    def countryName(self, val):
        self.set_component('C', val)
    @countryName.deleter
    def countryName(self):
        del self._comp['C']

    @property
    def orgName(self):
        """
        The organization name (O) component
        """
        return self.get_component('O')
    @orgName.setter
    def orgName(self, val):
        self.set_component('O', val)
    @orgName.deleter
    def orgName(self):
        del self._comp['O']

    @property
    def orgUnitName(self):
        """
        The organizational unit name (OU) component
        """
        return self.get_component('OU')
    @orgUnitName.setter
    def orgUnitName(self, val):
        self.set_component('OU', val)
    @orgUnitName.deleter
    def orgUnitName(self):
        del self._comp['OU']

    @property
    def commonName(self):
        """
        The common name (CN) component
        """
        return self.get_component('CN')
    @commonName.setter
    def commonName(self, val):
        self.set_component('CN', val)
    @commonName.deleter
    def commonName(self):
        del self._comp['CN']

    @classmethod
    def from_X509Name(cls, xname):
        """
        convert form an OpenSSL.crypto.X509Name
        """
        return DN(xname.get_components())

    @classmethod
    def from_str(cls, dnstr):
        """
        convert form an OpenSSL.crypto.X509Name
        """
        return DN(cls.parse_DN_str(dnstr))

    @staticmethod
    def parse_DN_str(subj):
        """
        convert a DN string into a list of 2-element tuples
        """
        fields = subj.split("/")
        return map(lambda f: tuple(f.split("=")), fields)


