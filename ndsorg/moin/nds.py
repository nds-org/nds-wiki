"""
The authentication module that integrates with the NDS User database
"""
from . import cilogon
from . import comanage

from MoinMoin.auth import BaseAuth
from MoinMoin.auth import CancelLogin, ContinueLogin
from MoinMoin.auth import MultistageFormLogin, MultistageRedirectLogin

class NDSAuth(BaseAuth):

    def __init__(self, client_key_file, client_secret_file,
                 cilogon_oauth_base=cilogon.cilogon_oauth_base):
        self._cilogon = cilogon.CILogonAuth(client_key_file, 
                                            client_secret_file, 
                                            cilogon_oauth_base)
        self._comanage = comanage.CoManageAuth()

    def login(self, request, user_obj, **kw):

        result = self._cilogon.login(request, user_obj, **kw)

        if not isinstance(result, ContinueLogin):
            return result

        user_obj = result.user_obj

        return self._comanage.login(request, user_obj, **kw)



