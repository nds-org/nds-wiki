"""
This module provides support specifically for the CI-Logon service.  

Specifically it supports its OAuth1 service and its certificate service 
as a means to authenticate and determine the identity (respectively) of 
the user.  
"""
from MoinMoin import log
logging = log.getLogger(__name__)

from MoinMoin import user
from MoinMoin.auth import BaseAuth
from MoinMoin.auth import CancelLogin, ContinueLogin
from MoinMoin.auth import MultistageFormLogin, MultistageRedirectLogin
from MoinMoin.auth import get_multistage_continuation_url
from OpenSSL import crypto

from .oauth1 import OAuth1

cilogon_oauth_base = "https://cilogon.org/oauth"

AUTH_USERNAME_TYPE = "AUTH_USERNAME_X509_DN"

def handle_cilogon_identity(user_obj, id_data):
    if hasattr(id_data, 'content'):
        id_data = id_data.content

    # The data is an X.509 certificate; use the distinguished name (DN)
    # as its authenticated name and let another module map it to a locally 
    # recognized identity.  

    auth_username = extract_DN(id_data)
    if auth_username:
        user_obj.auth_username = auth_username
        user_obj.valid = True

    return user_obj

def extract_DN(cert):
    """
    extract the cert's subject distinguished name as a string
    """
    subj = crypto.load_certificate(crypto.FILETYPE_PEM, cert).get_subject()
    return str(subj)[19:-2]

class CILogonAuth(OAuth1):
    """
    an authentication module that leverages CILogon's OAuth v.1a service and 
    its certificate service.  The former allows users to authenticate using 
    their home institution logins, the latter allows MoinMoin to descover
    their identity.
    """

    def __init__(self, client_key_file, client_secret_file, 
                 server_url_base=cilogon_oauth_base, require_success=True):

        # get keys from their files
        ckey = None
        csecret = None

        Oauth1.__init__(self, server_url_base, ckey, csecret,
                        handle_cilogon_identity, "initiate", "/delegate", 
                        "token", "getcert", require_success)



